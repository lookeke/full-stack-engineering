import Comm from './components/comm/comm'

/**
 * @returns JSXElement
 */
export default function App() {
	return <Comm />
}
